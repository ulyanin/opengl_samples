#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexVelocity;
layout(location = 2) in float oldParticleTime;

out float ratio;
out float velocity;

const float lifeTime = 15.0f;

void main()
{
    vec4 posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
	gl_Position = projectionMatrix * posCamSpace;
			
	ratio = oldParticleTime / lifeTime;
	velocity = length(vertexVelocity);

	gl_PointSize = (7.0 + ratio * 8.0) / -posCamSpace.z * 7;
}
