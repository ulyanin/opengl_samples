#include <Application.hpp>
#include <LightInfo.hpp>
#include <SkinnedMesh.h>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <Utils.h>

namespace
{
    float frand()
    {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    }

    //Удобная функция для вычисления цвета из линейной палитры от синего до красного
    glm::vec3 getColorFromLinearPalette(float value)
    {
        if (value < 0.25f)
        {
            return glm::vec3(0.0f, value * 4.0f, 1.0f);
        }
        else if (value < 0.5f)
        {
            return glm::vec3(0.0f, 1.0f, (0.5f - value) * 4.0f);
        }
        else if (value < 0.75f)
        {
            return glm::vec3((value - 0.5f) * 4.0f, 1.0f, 0.0f);
        }
        else
        {
            return glm::vec3(1.0f, (1.0f - value) * 4.0f, 0.0f);
        }
    }
}

/**
Пример cо скелетной анимацией.
*/
class SampleApplication : public Application
{
public:
	std::vector<SkinnedMeshPtr> _animMeshes;
	SkeletonPtr skeleton = std::make_shared<Skeleton>();

    //Идентификатор шейдерной программы
    ShaderProgramPtr _defaultShader;

	TexturePtr _spiderDiffuse;
    GLuint _sampler;

	const aiScene* assimpScene;

	int activeAnimation = 0;
	float animSpeed = 1.0f;

	float animNormalizedTime = 0.0f;
	bool animPlaying = false;

	// Just to orient our model.
	glm::quat modelOrientation = glm::quat(1, 1, -0.57, -0.67);

	void onStop() override {
		Application::onStop();
		aiReleaseImport(assimpScene);
	}

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей		
	    aiEnableVerboseLogging(true);
	    auto stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, nullptr);
	    aiAttachLogStream(&stream);

	    assimpScene = aiImportFile("models/Rig/Skeleton.fbx",
			    aiProcessPreset_TargetRealtime_MaxQuality);

	    assert(assimpScene);

	    // ----- Load meshes -----
	    for (int i = 0; i < assimpScene->mNumMeshes; i++) {
		    SkinnedMeshPtr skinnedMesh = std::make_shared<SkinnedMesh>();
		    skinnedMesh->setMesh(loadFromAIMesh(*assimpScene->mMeshes[i]));

		    /*
		     * Warning: if there are some not-skinned meshes in the scene
		     * they can be handled not correctly. At least they will not be drawn or drawn
		     * in unexpected place. They need to be detected and drawn be separate shader.
		     * But in this example we just don't care :)
		     */
		    if (assimpScene->mMeshes[i]->mNumBones) {
			    skinnedMesh->setSkeleton(skeleton);
			    skinnedMesh->setupSkeleton(*assimpScene->mMeshes[i]);
			    skinnedMesh->setBoneAttributes4(4, 3);
		    }

		    _animMeshes.push_back(skinnedMesh);
	    }

	    // ----- Load textures ----
	    _spiderDiffuse = loadTextureGL("images/Spider_diffuse.jpg");

	    // ----- Setup samplers -----
	    glCreateSamplers(1, &_sampler);
	    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_R, GL_REPEAT);
	    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
	    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

	    // ----- Init shaders ----
	    _defaultShader = std::make_shared<ShaderProgram>("shaders7/anim_dynamic.vert", "shaders7/anim_dynamic.frag");

	    std::cout << "Number of animations: " << assimpScene->mNumAnimations << std::endl;
	    std::cout << "Total bones: " << skeleton->boneTransforms.size() << std::endl;

	    printAssimpHierarchy(*assimpScene->mRootNode, "");

	    _cameraMover->setNearFarPlanes(0.1f, 1000.0f);
	    dynamic_cast<OrbitCameraMover &>(*_cameraMover).setOrientationParameters(400, 0, 0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

	        ImGui::SliderInt("Animation #", &activeAnimation, 0, assimpScene->mNumAnimations - 1);
	        ImGui::Text(assimpScene->mAnimations[activeAnimation]->mName.data);

	        ImGui::Checkbox("Playing", &animPlaying);
	        if (animPlaying) {
		        ImGui::DragFloat("Speed", &animSpeed, 0.001f, 0.01f, 5.0f);
	        }
	        ImGui::DragFloat("Normalized time", &animNormalizedTime, 0.01f, 0.0f, 1.0f);
        }
        ImGui::End();
    }

	/**
	 * Apply animation at specified time to skeleton bones local transformations.
	 */
	void animateSkeleton(Skeleton &skeleton, const aiAnimation &animation, float animationTime) {
		// A channel is responsible for a bone.
		for (unsigned int channel = 0; channel < animation.mNumChannels; channel++) {
			skeleton.animateLocalTransforms(*animation.mChannels[channel], animationTime);
		}
	}

	/**
	 * Now see how deep the rabbit hole is :)
	 */
	void printAssimpHierarchy(const aiNode& node, std::string shift) {
		std::cout << shift << node.mName.data << std::endl;
		shift += "|--";

		for (unsigned int i = 0; i < node.mNumChildren; i++)
			printAssimpHierarchy(*node.mChildren[i], shift);
	}

	void drawAssimpHierarchy(const aiNode& node) {
		for (unsigned int child = 0; child < node.mNumChildren; child++) {
			drawAssimpHierarchy(*node.mChildren[child]);
		}

		for (unsigned int i = 0; i < node.mNumMeshes; i++) {
			unsigned int meshIndex = node.mMeshes[i];
			if (_animMeshes[meshIndex])
				_animMeshes[meshIndex]->getMesh()->draw();
		}
	}

	void fillSkeletonMatrices(Skeleton &skeleton, const aiNode& node, glm::mat4 parentModelMatrix) {
		glm::mat4 nodeLocalMatrix = Utils::aiMatrixToGLM(node.mTransformation);

		glm::mat4 nodeModelMatrix = parentModelMatrix * nodeLocalMatrix;

		// Traverse children.
		for (unsigned int child = 0; child < node.mNumChildren; child++) {
			fillSkeletonMatrices(skeleton, *node.mChildren[child], nodeModelMatrix);
		}

		std::string nodeName(node.mName.data);
		int boneIndex = skeleton.findBone(nodeName);
		if (boneIndex >= 0)
			skeleton[boneIndex] = nodeModelMatrix * skeleton.getBoneInfo(boneIndex).offsetMatrix;
	}

	void update()
	{
		Application::update();

		static double last_time = glfwGetTime();
		double time = glfwGetTime();
		double deltaTime = time - last_time;
		last_time = time;

		const aiAnimation &anim = *assimpScene->mAnimations[activeAnimation];
		if (animPlaying)
			animNormalizedTime = (float)fmod(animNormalizedTime + deltaTime * animSpeed, 1);
		float animTime = (float)(animNormalizedTime * anim.mDuration);

		// Count local bone transforms based on time.
		animateSkeleton(*skeleton, *assimpScene->mAnimations[activeAnimation], animTime);
		// Apply hierarchy to deduce global transforms.
		skeleton->recountGlobalTransforms(*assimpScene->mRootNode);
	}

    void draw() override
    {
	    GLsizei width, height;
	    glfwGetFramebufferSize(_window, &width, &height);

	    glViewport(0, 0, width, height);
	    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	    glDisable(GL_CULL_FACE);

	    glm::mat4 PV = _camera.projMatrix * _camera.viewMatrix;
	    glm::mat4 modelMatrix = glm::mat4_cast(glm::normalize(modelOrientation));

	    _defaultShader->use();

	    glActiveTexture(GL_TEXTURE0);
	    _spiderDiffuse->bind();
	    glBindSampler(0, _sampler);
	    _defaultShader->setIntUniform("samplerDiffuse", 0);
	    _defaultShader->setMat4Uniform("PVM", PV * modelMatrix);

	    _defaultShader->setMat4Uniforms("boneMatrices[0]", skeleton->boneTransforms);

	    drawAssimpHierarchy(*assimpScene->mRootNode);

	    glBindSampler(0,0);
	    glUseProgram(0);
	    glBindVertexArray(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}