#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <glm/gtc/quaternion.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <deque>

int rand(int N) {
	return rand() % N;
}

int rand(int a, int b) {
	return a + rand(b - a);
}

float frand() {
	return (float)rand() / std::numeric_limits<int>::max();
}

float frand(float a, float b) {
	return a + frand() * (b - a);
}

glm::vec3 frand(glm::vec3 a, glm::vec3 b) {
	return glm::mix(a, b, glm::vec3(frand(), frand(), frand()));
}

/**
Пример использования layered rendering.
*/
class SampleApplication : public Application
{
public:
	MeshPtr sphere;
	MeshPtr bunny;
	MeshPtr cube;
	std::vector<MeshPtr> meshes;

	TexturePtr brickTex;
	TexturePtr earthTex;
	TexturePtr grassTex;
	TexturePtr chessTex;
	std::vector<TexturePtr> textures;

	MeshPtr screenQuad;
	MeshPtr _backgroundCube;
	std::vector<glm::mat4> cubeViewMatrices;
	std::vector<glm::mat3> cubeViewNormalMatrices;

	float cubeNearPlane = 0.01f;
	float cubeFarPlane = 100.0f;

	GLuint _samplerMIP;
	GLuint _samplerCube;
	GLuint _samplerCubeMIP;

	float mipBias = 0.0f;

	ShaderProgramPtr captureCubeShader;
	ShaderProgramPtr debugCubemapShader;
	ShaderProgramPtr commonShader;
	ShaderProgramPtr envMappingShader;
	ShaderProgramPtr _skyboxShader;

	//Для преобразования координат в текстурные координаты нужна специальная матрица
	glm::mat3 cubemapTextureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	const GLsizei cubemapResolution = 1024; // critical for image copy
	TexturePtr cubeTex;
	TexturePtr cubeDepthTex;
	GLuint cubeFBO;

	std::vector<GLuint> cubePerFaceFBO;

	GLsizei skyboxResolution;
	TexturePtr skyboxTex;

	LightInfo _light;
	//Переменные для управления положением одного источника света
	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	struct Model {
		MeshPtr mesh;
		TexturePtr texture;
		glm::vec3 position;
		glm::quat orientation;
	};

	Model centralSphere;
	std::vector<Model> scene;

	std::vector<glm::vec3> planetInitialPoses;
	std::vector<glm::vec3> planetRotSpeeds;

	bool debugCubemap = false;
	bool cubemapMipmapFiltering = false;
	bool cubemapSeamless = false;
	bool layeredRendering = false;

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}

			ImGui::Checkbox("Debug cubemap", &debugCubemap);
			ImGui::Checkbox("Cubemap MIP", &cubemapMipmapFiltering);
			if (cubemapMipmapFiltering) {
				if (ImGui::SliderFloat("MIP bias", &mipBias, -5.0f, 5.0f)) {
					glSamplerParameterf(_samplerCubeMIP, GL_TEXTURE_LOD_BIAS, mipBias);
				}
				if (ImGui::Checkbox("Seamless", &cubemapSeamless)) {
					if (cubemapSeamless) {
						glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
					}
					else {
						glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
					}
				}
			}
			ImGui::Checkbox("Layered rendering", &layeredRendering);
		}
		ImGui::End();
	}

	void setupCubeFBOAndTexture(GLsizei texSize) {
		cubeTex = std::make_shared<Texture>(GL_TEXTURE_CUBE_MAP);
		cubeDepthTex = std::make_shared<Texture>(GL_TEXTURE_CUBE_MAP);
		if (GLEW_ARB_texture_storage) {
			cubeTex->initStorage2D(6, GL_RGB8, texSize, texSize);
			cubeDepthTex->initStorage2D(1, GL_DEPTH_COMPONENT24, texSize, texSize);
		}
		else {
			for (GLuint i = 0; i < 6; i++) {
				cubeTex->setTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB8, texSize, texSize, GL_RGB,
						GL_UNSIGNED_BYTE, nullptr);
				cubeDepthTex->setTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT24, texSize, texSize, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, nullptr);
			}
			cubeTex->generateMipmaps();
		}

		// Construct the framebuffer.
		if (USE_DSA) {
			glCreateFramebuffers(1, &cubeFBO);
			glNamedFramebufferTexture(cubeFBO, GL_COLOR_ATTACHMENT0, cubeTex->texture(), 0);
			glNamedFramebufferTexture(cubeFBO, GL_DEPTH_ATTACHMENT, cubeDepthTex->texture(), 0);
		}
		else {
			glGenFramebuffers(1, &cubeFBO);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, cubeFBO);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, cubeTex->texture(), 0);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, cubeDepthTex->texture(), 0);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		}

		// Construct per-face fbos.
		cubePerFaceFBO.resize(6);
		if (USE_DSA) {
			glCreateFramebuffers(6, cubePerFaceFBO.data());
			for (int i = 0; i < 6; i++) {
				glNamedFramebufferTextureLayer(cubePerFaceFBO[i], GL_COLOR_ATTACHMENT0, cubeTex->texture(), 0, i);
				glNamedFramebufferTextureLayer(cubePerFaceFBO[i], GL_DEPTH_ATTACHMENT, cubeDepthTex->texture(), 0, i);
			}
		}
		else {
			assert(false);
		}
	}

	void genScene(std::vector<Model> &_scene) {
		const size_t numObjects = 10;

		for (size_t i = 0; i < numObjects; i++) {
			Model model;
			model.texture = textures[rand(textures.size())];
			model.mesh = meshes[rand(meshes.size())];
			model.position = frand(glm::vec3(-10, -10, -10), glm::vec3(10, 10, 10));
			model.orientation = glm::normalize(glm::quat(frand(), frand(), frand(), frand()));
			_scene.push_back(model);

			planetInitialPoses.push_back(model.position);
			planetRotSpeeds.push_back(glm::normalize(glm::vec3(frand(), frand(), frand())) * frand());
		}
	}

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей

		sphere = makeSphere(0.5f);
		cube = makeCube(0.5f);
		bunny = loadFromFile("models/bunny.obj");
		meshes = { sphere, cube, bunny };

		screenQuad = makeScreenAlignedQuad();

		//=========================================================
		// Загрузка текстур.
		brickTex = loadTextureGL("images/brick.jpg");
		earthTex = loadTextureGL("images/earth_global.jpg");
		grassTex = loadTextureGL("images/grass.jpg");
		chessTex = loadTextureDDS("images/chess.dds");
		textures = { brickTex, earthTex, grassTex, chessTex };

		// Кубическая текстура и слоёный буфер кадра.
		setupCubeFBOAndTexture(cubemapResolution);
		skyboxTex = loadCubeTexture("images/cube");
		_backgroundCube = makeCube(10.0f);

		//=========================================================
		//Инициализация значений переменных освщения
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);

		//=========================================================
		//Инициализация шейдеров
		captureCubeShader = std::make_shared<ShaderProgram>("shaders9/GS_layered/capture_cube.vert", "shaders9/GS_layered/capture_cube.geom", "shaders9/GS_layered/capture_cube.frag");
		debugCubemapShader = std::make_shared<ShaderProgram>("shaders9/GS_layered/debug_cubemap.vert", "shaders9/GS_layered/debug_cubemap.frag");
		commonShader = std::make_shared<ShaderProgram>("shaders/common.vert", "shaders/common.frag");
		envMappingShader = std::make_shared<ShaderProgram>("shaders/common.vert", "shaders9/GS_layered/env_mapping.frag");
		_skyboxShader = std::make_shared<ShaderProgram>("shaders/skybox.vert", "shaders/skybox.frag");

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &_samplerMIP);
		glSamplerParameteri(_samplerMIP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_samplerMIP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_samplerMIP, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerMIP, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerCube);
		glSamplerParameteri(_samplerCube, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_samplerCube, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_samplerCube, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_samplerCube, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_samplerCube, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


		glGenSamplers(1, &_samplerCubeMIP);
		glSamplerParameteri(_samplerCubeMIP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_samplerCubeMIP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_samplerCubeMIP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_samplerCubeMIP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_samplerCubeMIP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		genScene(scene);
		centralSphere = { sphere, earthTex, glm::vec3(0), glm::quat(1,0,0,0) };

		_cameraMover = std::make_shared<FreeCameraMover>();

		skyboxTex->getSize(skyboxResolution, skyboxResolution);
		std::cout << "Skybox face size: " << skyboxResolution << "x" << skyboxResolution << std::endl;
	}


	void update()
	{
		Application::update();

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;

		float time = static_cast<float>(glfwGetTime());
		float deltaTime = time - _oldTime;
		_oldTime = time;

		for (size_t i = 0; i < scene.size(); i++) {
			scene[i].position = glm::rotate(glm::quat(1,0,0,0), time * glm::length(planetRotSpeeds[i]), glm::normalize(planetRotSpeeds[i])) * planetInitialPoses[i];
//			scene[i].orientation = glm::rotate(scene[i].orientation, 0.1f * deltaTime * glm::length(selfRotSpeeds[i]), glm::normalize(selfRotSpeeds[i]));
		}
	}

	void drawScene(ShaderProgramPtr _shader, std::function<void(glm::mat4)> modelMatrixHandler) {
		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _samplerMIP);
		_shader->setIntUniform("diffuseTex", 0);
		for (auto model : scene) {
//			glm::mat4 modelMatrix = glm::translate(glm::mat4_cast(model.orientation), model.position);
			glm::mat4 modelMatrix = glm::translate(glm::mat4(1), model.position) * glm::mat4_cast(model.orientation);
			modelMatrixHandler(modelMatrix);
			model.texture->bind();
			model.mesh->draw();
		}
	}
	
	void genCubeViews(glm::vec3 center, std::vector<glm::mat4> &viewMatrices) {
		viewMatrices.resize(6);
		const int offset = GL_TEXTURE_CUBE_MAP_POSITIVE_X;

		viewMatrices[GL_TEXTURE_CUBE_MAP_POSITIVE_X - offset] = glm::lookAt(center, center + glm::vec3(1,0,0), glm::vec3(0,-1,0));
		viewMatrices[GL_TEXTURE_CUBE_MAP_NEGATIVE_X - offset] = glm::lookAt(center, center + glm::vec3(-1,0,0), glm::vec3(0,-1,0));
		viewMatrices[GL_TEXTURE_CUBE_MAP_POSITIVE_Y - offset] = glm::lookAt(center, center + glm::vec3(0,1,0), glm::vec3(0,0,1));
		viewMatrices[GL_TEXTURE_CUBE_MAP_NEGATIVE_Y - offset] = glm::lookAt(center, center + glm::vec3(0,-1,0), glm::vec3(0,0,-1));
		viewMatrices[GL_TEXTURE_CUBE_MAP_POSITIVE_Z - offset] = glm::lookAt(center, center + glm::vec3(0,0,1), glm::vec3(0,-1,0));
		viewMatrices[GL_TEXTURE_CUBE_MAP_NEGATIVE_Z - offset] = glm::lookAt(center, center + glm::vec3(0,0,-1), glm::vec3(0,-1,0));

		for (int i = 0; i < 6; i++) {
			viewMatrices[i] = viewMatrices[i] * glm::mat4(cubemapTextureMatrix);
		}
	}

	void sendFragmentShaderUniforms(ShaderProgramPtr _shader, const std::vector<glm::mat4> &viewMatrices) {
		std::vector<glm::vec3> lightPositionsCamSpace(viewMatrices.size());
		for (size_t i = 0; i < viewMatrices.size(); i++) {
			lightPositionsCamSpace[i] = glm::vec3(viewMatrices[i] * glm::vec4(_light.position, 1.0));
		}

		if (lightPositionsCamSpace.size() > 1)
			_shader->setVec3Uniforms("light.pos[0]", lightPositionsCamSpace);
		else
			_shader->setVec3Uniform("light.pos", lightPositionsCamSpace[0]);

		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);
	}

	void reinitCubemap() {
		if (GLEW_ARB_copy_image) {
			// 2048 -> 1024
			glCopyImageSubData(
					skyboxTex->texture(), GL_TEXTURE_CUBE_MAP, 1, 0, 0, 0,
					cubeTex->texture(), GL_TEXTURE_CUBE_MAP, 0, 0, 0, 0,
					cubemapResolution, cubemapResolution, 6);
		}
	}

	void drawToCubemap(glm::vec3 center) {
		captureCubeShader->use();

		genCubeViews(center, cubeViewMatrices);
		cubeViewNormalMatrices.resize(6);
		for (size_t i = 0; i < 6; i++) {
			cubeViewNormalMatrices[i] = glm::mat3(cubeViewMatrices[i]);
		}

		captureCubeShader->setMat4Uniforms("faceV[0]", cubeViewMatrices);
		captureCubeShader->setMat3Uniforms("faceV_normals[0]", cubeViewNormalMatrices);

		glm::mat4 cubeProj = glm::perspective(glm::radians(90.0f), 1.0f, cubeNearPlane, cubeFarPlane);
		captureCubeShader->setMat4Uniform("P", cubeProj);

		sendFragmentShaderUniforms(captureCubeShader, cubeViewMatrices);

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, cubeFBO);
		glViewport(0, 0, cubemapResolution, cubemapResolution);
		glClear(GL_DEPTH_BUFFER_BIT | (GLEW_ARB_copy_image ? 0U : GL_COLOR_BUFFER_BIT));

		drawScene(captureCubeShader, [&](glm::mat4 M) {
			captureCubeShader->setMat4Uniform("M", M);
			captureCubeShader->setMat3Uniform("M_normals", glm::inverse(glm::transpose(glm::mat3(M))));
		});
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}

	void drawToCubeMapOldStyle(glm::vec3 center) {
		commonShader->use();

		genCubeViews(center, cubeViewMatrices);

		glm::mat4 cubeProj = glm::perspective(glm::radians(90.0f), 1.0f, cubeNearPlane, cubeFarPlane);
		commonShader->setMat4Uniform("projectionMatrix", cubeProj);

		for (int face = 0; face < 6; face++) {
			commonShader->setMat4Uniform("viewMatrix", cubeViewMatrices[face]);
			sendFragmentShaderUniforms(commonShader, {cubeViewMatrices[face]});

			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, cubePerFaceFBO[face]);
			glViewport(0, 0, cubemapResolution, cubemapResolution);
			glClear(GL_DEPTH_BUFFER_BIT | (GLEW_ARB_copy_image ? 0U : GL_COLOR_BUFFER_BIT));

			glm::mat4 V = cubeViewMatrices[face];
			drawScene(commonShader, [&, V](glm::mat4 M) {
				commonShader->setMat4Uniform("modelMatrix", M);
				commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(V * M))));
			});
		}
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}

	void drawDebugCubemap() {
		debugCubemapShader->use();

		glViewport(0, 0, 500, 250);

		glActiveTexture(GL_TEXTURE1);
		glBindSampler(1, _samplerCube);
		cubeTex->bind();
		debugCubemapShader->setIntUniform("cubemap", 1);

		glDisable(GL_DEPTH_TEST);
		screenQuad->draw();
		glEnable(GL_DEPTH_TEST);
	}

	void drawCommon() {
		GLsizei width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		glViewport(0, 0, width, height);

		commonShader->use();

		commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		sendFragmentShaderUniforms(commonShader, { _camera.viewMatrix });

		drawScene(commonShader, [&] (glm::mat4 M) {
			commonShader->setMat4Uniform("modelMatrix", M);
			commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * M))));
		});
	}

	void drawEnvMapping() {
		if (cubemapMipmapFiltering) {
			cubeTex->generateMipmaps();
		}

		GLsizei width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		glViewport(0, 0, width, height);

		envMappingShader->use();

		envMappingShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		envMappingShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		sendFragmentShaderUniforms(envMappingShader, { _camera.viewMatrix });

		glm::mat3 V_inv = cubemapTextureMatrix * glm::inverse(glm::mat3(_camera.viewMatrix));
		envMappingShader->setMat3Uniform("V_inv", V_inv);

		glActiveTexture(GL_TEXTURE1);
		glBindSampler(1, cubemapMipmapFiltering ? _samplerCubeMIP : _samplerCube);
		cubeTex->bind();
		envMappingShader->setIntUniform("cubeTex", 1);

		auto Mhandler = [&] (glm::mat4 M) {
			envMappingShader->setMat4Uniform("modelMatrix", M);
			envMappingShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * M))));
		};
		Mhandler(glm::translate(glm::mat4(1), centralSphere.position));

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _samplerMIP);
		centralSphere.texture->bind();
		envMappingShader->setIntUniform("diffuseTex", 0);
		centralSphere.mesh->draw();
//		drawScene(envMappingShader, Mhandler);
	}

	void drawBackgroundCubemap() {
		_skyboxShader->use();

		glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]);

		_skyboxShader->setVec3Uniform("cameraPos", cameraPos);
		_skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_skyboxShader->setMat3Uniform("textureMatrix", cubemapTextureMatrix);

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _samplerCube);
		skyboxTex->bind();
		_skyboxShader->setIntUniform("cubeTex", 0);

		glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

		_backgroundCube->draw();

		glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
	}

	void draw() override
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		drawBackgroundCubemap();

		//-------------------------
		reinitCubemap();
		if (layeredRendering) {
			drawToCubemap(centralSphere.position);
		}
		else {
			drawToCubeMapOldStyle(centralSphere.position);
		}

		drawCommon();
		drawEnvMapping();

		if (debugCubemap) {
			drawDebugCubemap();
		}

		//-------------------------

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler(0, 0);
		glBindSampler(1, 0);
		glUseProgram(0);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}