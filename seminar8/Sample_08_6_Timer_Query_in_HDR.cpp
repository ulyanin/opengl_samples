#include <Application.hpp>
#include <LightInfo.hpp>
#include <Framebuffer.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <QueryObject.h>

#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

/*
 * Пример использования Occlusion Query для замеров производительности на основе примера с HDR.
 */

class QueryResultProcessor {
public:
	virtual ~QueryResultProcessor() { }
	virtual double getValue() const = 0;
	virtual void clear(double value) = 0;
	virtual void pushResult(double value) = 0;
};

class HistoryLastRecord : public QueryResultProcessor {
public:
	double getValue() const override {
		return lastValue;
	}
	void clear(double value) override {
		lastValue = value;
	}
	void pushResult(double value) override {
		lastValue = value;
	}
protected:
	double lastValue = 0.0;
};

class HistoryAverager : public QueryResultProcessor {
public:
	HistoryAverager(size_t N) {
        results = std::vector<double>(N);
		clear(0.0);
	}

	double getValue() const override {
        if (changed) {
            double sum = 0.0;
            for (size_t i = 0; i < results.size(); i++)
                sum += results[i];
            avg = sum / results.size();
            changed = false;
        }
        return avg;
	}

	void clear(double result) override {
		for (size_t i = 0; i < results.size(); i++)
			results[i] = result;
        changed = true;
	}

	void pushResult(double result) override {
		results[index] = result;
		++index;
        if (index == results.size())
            index = 0;
        changed = true;
	}
protected:
    std::vector<double> results;
    mutable double avg = 0.0;
    mutable bool changed = true;
	int index = 0;
};

class QueryCounter {
private:
	void subscribeForQueryManager() {
		queryManager.setQueryResultHandler(std::bind(&QueryCounter::onQueryResultReady, this, std::placeholders::_1));
	}
public:
    void onQueryResultReady(const QueryObjectPtr &occlusionQuery) {
        history->pushResult(static_cast<double>(occlusionQuery->getResultSync()));
    }

	explicit QueryCounter(std::shared_ptr<QueryResultProcessor> _history, QueryObject::Target target) : history(
			std::move(_history)), queryManager(target) {
        // If we put 1 here, the number of lost queries will grow every frame.
		queryManager.setMaxPendingQueries(2);
		subscribeForQueryManager();
	}
	QueryCounter(const QueryCounter &) = delete;
	QueryCounter &operator=(const QueryCounter) = delete;

	QueryCounter(QueryCounter &&qc) : history(std::move(qc.history)), queryManager(std::move(qc.queryManager)) {
		subscribeForQueryManager();
	}
	
	QueryCounter &operator=(QueryCounter &&qc) {
		queryManager.setQueryResultHandler(nullptr);
		history = std::move(qc.history);
		queryManager = std::move(qc.queryManager);
		subscribeForQueryManager();
		return *this;
	}

	double getValue() {
		return history->getValue();
	}

    void clearHistory() {
        history->clear(0.0);
    }

	QueryManager &getQueryManager() { return queryManager; }
	const QueryManager &getQueryManager() const { return queryManager; }
protected:
	std::shared_ptr<QueryResultProcessor> history;
	QueryManager queryManager;
};

class SampleApplication : public Application
{
public:
    MeshPtr _cube;
    MeshPtr _sphere;
    MeshPtr _bunny;
    MeshPtr _ground;

    MeshPtr _quad;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _quadDepthShader;
    ShaderProgramPtr _quadColorShader;
    ShaderProgramPtr _renderToShadowMapShader;
    ShaderProgramPtr _renderToGBufferShader;
    ShaderProgramPtr _renderDeferredShader;
    ShaderProgramPtr _brightShader;
    ShaderProgramPtr _horizBlurShader;
    ShaderProgramPtr _vertBlurShader;
	ShaderProgramPtr _horizBlurOptShader;
	ShaderProgramPtr _vertBlurOptShader;
    ShaderProgramPtr _toneMappingShader;

    QueryCounter gbufferTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter shadowTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter lightTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter brightTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
	QueryCounter horizBlurTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
	QueryCounter vertBlurTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter tonemapTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter finalRenderTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);
    QueryCounter debugTC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryAverager>(120)), QueryObject::QOT_TIME_ELAPSED);

	bool _countSamples = false;
	bool _countPrimitives = false;

	QueryCounter gbufferSC = QueryCounter(std::static_pointer_cast<QueryResultProcessor>(std::make_shared<HistoryLastRecord>()), QueryObject::QOT_SAMPLES_PASSED);
	QueryCounter gbufferPC = QueryCounter(std::make_shared<HistoryLastRecord>(), QueryObject::QOT_PRIMITIVES_GENERATED);

	enum PerStepSyncType {
		SyncNone,
		SyncFlush,
		SyncFinish
	};

	PerStepSyncType stepSyncType = SyncNone;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = 0.0;
    float _theta = 0.48;

    float _lightIntensity = 1.0;
    LightInfo _light;
    CameraInfo _lightCamera;

    TexturePtr _brickTex;

    GLuint _sampler;
    GLuint _repeatSampler;
    GLuint _depthSampler;

    bool _applyEffect = true;

    bool _showGBufferDebug = false;
    bool _showShadowDebug = false;
    bool _showDeferredDebug = false;
    bool _showBloomDebug = false;

    bool _optimizeUnusedStages = false;
	bool _optimizeBlur = false; // Оптимизация Gaussian blur за счёт билинейной фильтрации.

    float _exposure = 1.0f; //Параметр алгоритма ToneMapping
    float _brightnessThreshold = 1.0f; // Порог яркости для применения размытия.
	float _bloomFactor = 5.0f; // Фактор учёта заблуренной яркости.

    FramebufferPtr _gbufferFB;
    TexturePtr _depthTex;
    TexturePtr _normalsTex;
    TexturePtr _diffuseTex;

    FramebufferPtr _shadowFB;
    TexturePtr _shadowTex;

    FramebufferPtr _deferredFB;
    TexturePtr _deferredTex;

    FramebufferPtr _brightFB;
    TexturePtr _brightTex;

    FramebufferPtr _horizBlurFB;
    TexturePtr _horizBlurTex;

    FramebufferPtr _vertBlurFB;
    TexturePtr _vertBlurTex;

    FramebufferPtr _toneMappingFB;
    TexturePtr _toneMappingTex;

    //Старые размеры экрана
    int _oldWidth = 1024;
    int _oldHeight = 1024;

    void initFramebuffers()
    {
        //Создаем фреймбуфер для рендеринга в G-буфер
        _gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

        _normalsTex = _gbufferFB->addBuffer(GL_RGB16F, GL_COLOR_ATTACHMENT0);
        _diffuseTex = _gbufferFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT1);
        _depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _gbufferFB->initDrawBuffers();

        if (!_gbufferFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для рендеринга в теневую карту

        _shadowFB = std::make_shared<Framebuffer>(1024, 1024);

        _shadowTex = _shadowFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _shadowFB->initDrawBuffers();

        if (!_shadowFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для результатов расчета освещения

        _deferredFB = std::make_shared<Framebuffer>(1024, 1024);

        _deferredTex = _deferredFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);
	    /*
	     * If you enable the commented variant there will be no difference for FINAL RENDER step Occlusion Query timing
	     * with FLUSH per-step sync enabled.
	     */
//	    _deferredTex = _deferredFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);

        _deferredFB->initDrawBuffers();

        if (!_deferredFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Фреймбуфер с текстурой, куда будут записаны только самые яркие фрагменты

        _brightFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _brightTex = _brightFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _brightFB->initDrawBuffers();

        if (!_brightFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _horizBlurFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _horizBlurTex = _horizBlurFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _horizBlurFB->initDrawBuffers();

        if (!_horizBlurFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _vertBlurFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _vertBlurTex = _vertBlurFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _vertBlurFB->initDrawBuffers();

        if (!_vertBlurFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _toneMappingFB = std::make_shared<Framebuffer>(1024, 1024);

        _toneMappingTex = _toneMappingFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);

        _toneMappingFB->initDrawBuffers();

        if (!_toneMappingFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }
    }

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей		

        _cube = makeCube(0.5f);
        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _sphere = makeSphere(0.5f);
        _sphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _bunny = loadFromFile("models/bunny.obj");
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        _ground = makeGroundPlane(5.0f, 2.0f);

        _quad = makeScreenAlignedQuad();

        //=========================================================
        //Инициализация шейдеров

        _quadDepthShader = std::make_shared<ShaderProgram>("shaders/quadDepth.vert", "shaders/quadDepth.frag");
        _quadColorShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders/quadColor.frag");
        _renderToShadowMapShader = std::make_shared<ShaderProgram>("shaders6/toshadow.vert", "shaders6/toshadow.frag");
        _renderToGBufferShader = std::make_shared<ShaderProgram>("shaders7/togbuffer.vert", "shaders7/togbuffer.frag");
        _renderDeferredShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/deferred.frag");
        _brightShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/bright.frag");
        _horizBlurShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/horizblur.frag");
        _vertBlurShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/vertblur.frag");
	    _horizBlurOptShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/horizblur_opt.frag");
	    _vertBlurOptShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/vertblur_opt.frag");
        _toneMappingShader = std::make_shared<ShaderProgram>("shaders/quadColor.vert", "shaders8/tonemapping.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _brickTex = loadTexture("images/brick.jpg", SRGB::YES); //sRGB

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glGenSamplers(1, &_repeatSampler);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

        glGenSamplers(1, &_depthSampler);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        //=========================================================
        glfwGetFramebufferSize(_window, &_oldWidth, &_oldHeight);

        //=========================================================
        //Инициализация фреймбуфера для рендера теневой карты

        initFramebuffers();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

                ImGui::SliderFloat("intensity", &_lightIntensity, 0.0f, 5.0f);
            }

            ImGui::Checkbox("Optimized unused stages", &_optimizeUnusedStages);
            ImGui::Checkbox("Apply HDR", &_applyEffect);
	        ImGui::Checkbox("Optimize blur", &_optimizeBlur);

            // HDR setup.
            if (ImGui::CollapsingHeader("HDR setup")) {
                ImGui::SliderFloat("Brightness threshold", &_brightnessThreshold, 0.01f, 10.0f);
                ImGui::SliderFloat("Bloom factor", &_bloomFactor, 0.1f, 10.0f);
                ImGui::SliderFloat("Exposure", &_exposure, 0.01f, 10.0f);
            }

            // Debug output.
            if (ImGui::CollapsingHeader("Debug")) {
                ImGui::Checkbox("Show G-buffer debug", &_showGBufferDebug);
                ImGui::Checkbox("Show shadow debug", &_showShadowDebug);
                ImGui::Checkbox("Show deferred debug", &_showDeferredDebug);
                ImGui::Checkbox("Show bloom debug", &_showBloomDebug);
            }

	        if (ImGui::CollapsingHeader("Occlusion & Primitive query")) {
		        ImGui::Checkbox("Count samples", &_countSamples);
		        ImGui::Checkbox("Count primitives", &_countPrimitives);
		        if (_countSamples) {
			        gbufferSC.getQueryManager().processFinishedQueries();
			        ImGui::LabelText("G-buffer samples", "%.0f", gbufferSC.getValue());
		        }
		        if (_countPrimitives) {
			        gbufferPC.getQueryManager().processFinishedQueries();
			        ImGui::LabelText("G-buffer primitives", "%.0f", gbufferPC.getValue());
		        }
	        }

	        // Timing output.
            if (ImGui::CollapsingHeader("Timing"/*, ImGuiTreeNodeFlags_DefaultOpen*/)) {
	            ImGui::RadioButton("No sync", reinterpret_cast<int*>(&stepSyncType), static_cast<int>(SyncNone));
	            ImGui::RadioButton("Flush", reinterpret_cast<int*>(&stepSyncType), static_cast<int>(SyncFlush));
	            ImGui::RadioButton("Finish", reinterpret_cast<int*>(&stepSyncType), static_cast<int>(SyncFinish));

                const char *timeMsFormat = "%5.0f (%.2f)";

	            gbufferTC.getQueryManager().processFinishedQueries();
	            shadowTC.getQueryManager().processFinishedQueries();
	            lightTC.getQueryManager().processFinishedQueries();
	            brightTC.getQueryManager().processFinishedQueries();
	            vertBlurTC.getQueryManager().processFinishedQueries();
	            horizBlurTC.getQueryManager().processFinishedQueries();
	            tonemapTC.getQueryManager().processFinishedQueries();
	            finalRenderTC.getQueryManager().processFinishedQueries();
	            debugTC.getQueryManager().processFinishedQueries();

                double total = gbufferTC.getValue() + shadowTC.getValue() + lightTC.getValue() +
		                brightTC.getValue() + vertBlurTC.getValue() + horizBlurTC.getValue() +
		                tonemapTC.getValue() + finalRenderTC.getValue() + debugTC.getValue();

                ImGui::LabelText("GBuffer, ms(part)", timeMsFormat, gbufferTC.getValue() * 1e-3,
		                gbufferTC.getValue() / total);
                ImGui::LabelText("Shadow, ms(part)", timeMsFormat, shadowTC.getValue() * 1e-3,
		                shadowTC.getValue() / total);
                ImGui::LabelText("Light, ms(part)", timeMsFormat, lightTC.getValue() * 1e-3,
		                lightTC.getValue() / total);
                ImGui::LabelText("Bright, ms(part)", timeMsFormat, brightTC.getValue() * 1e-3,
		                brightTC.getValue() / total);
	            ImGui::LabelText("HBlur, ms(part)", timeMsFormat, horizBlurTC.getValue() * 1e-3,
			            horizBlurTC.getValue() / total);
	            ImGui::LabelText("VBlur, ms(part)", timeMsFormat, vertBlurTC.getValue() * 1e-3,
			            vertBlurTC.getValue() / total);
                ImGui::LabelText("Tonemap, ms(part)", timeMsFormat, tonemapTC.getValue() * 1e-3,
		                tonemapTC.getValue() / total);
                ImGui::LabelText("Final render, ms(part)", timeMsFormat, finalRenderTC.getValue() * 1e-3,
		                finalRenderTC.getValue() / total);
                ImGui::LabelText("Debug, ms", timeMsFormat, debugTC.getValue() * 1e-3,
		                debugTC.getValue() / total);

                ImGui::Spacing();

                total *= 1e-3; // to ms
                ImGui::LabelText("Total, ms", "%5.0f", total);
                ImGui::LabelText("Estimated fps", "%.0f", 1e6 / total);

	            ImGui::Spacing();
				// Occlusion queries manager stats.
//	            {
//		            QueryManager::Stats brightStats = brightTC.getQueryManager().getStats();
//		            ImGui::LabelText("Bright OQ stats", "B=%ld; H=%ld; L=%ld", brightStats.queriesBegan,
//				            brightStats.queriesHandled, brightStats.queriesLost);
//		            if (brightStats.queriesBegan > 1000)
//			            brightTC.getQueryManager().clearStats();
//
//	            }
                {
                    QueryManager::Stats hblurStats = horizBlurTC.getQueryManager().getStats();
                    ImGui::LabelText("HBlur OQ stats", "B=%ld; H=%ld; L=%ld", hblurStats.queriesBegan,
                            hblurStats.queriesHandled, hblurStats.queriesLost);
                    if (hblurStats.queriesBegan > 1000)
	                    horizBlurTC.getQueryManager().clearStats();
                }
                {
                    QueryManager::Stats vblurStats = vertBlurTC.getQueryManager().getStats();
                    ImGui::LabelText("VBlur OQ stats", "B=%ld; H=%ld; L=%ld", vblurStats.queriesBegan,
                            vblurStats.queriesHandled, vblurStats.queriesLost);
                    if (vblurStats.queriesBegan > 1000)
	                    vertBlurTC.getQueryManager().clearStats();
                }
//	            {
//		            QueryManager::Stats finalRenderStats = finalRenderTC.getQueryManager().getStats();
//		            ImGui::LabelText("Final OQ stats", "B=%ld; H=%ld; L=%ld", finalRenderStats.queriesBegan,
//				            finalRenderStats.queriesHandled, finalRenderStats.queriesLost);
//		            if (finalRenderStats.queriesBegan > 1000)
//			            finalRenderTC.getQueryManager().clearStats();
//
//	            }
            }
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                _applyEffect = !_applyEffect;
            }
            else if (key == GLFW_KEY_Z)
            {
                _showGBufferDebug = !_showGBufferDebug;
            }
            else if (key == GLFW_KEY_X)
            {
                _showShadowDebug = !_showShadowDebug;
            }
            else if (key == GLFW_KEY_C)
            {
                _showDeferredDebug = !_showDeferredDebug;
            }
            else if (key == GLFW_KEY_V)
            {
                _showBloomDebug = !_showBloomDebug;
            }
        }
    }

    void update()
    {
        Application::update();

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _lightCamera.viewMatrix = glm::lookAt(_light.position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);

        //Если размер окна изменился, то изменяем размеры фреймбуферов - перевыделяем память под текстуры

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        if (width != _oldWidth || height != _oldHeight)
        {
            _gbufferFB->resize(width, height);
            _deferredFB->resize(width, height);

            _brightFB->resize(width / 2, height / 2);
            _horizBlurFB->resize(width / 2, height / 2);
            _vertBlurFB->resize(width / 2, height / 2);
            _toneMappingFB->resize(width, height);

            _oldWidth = width;
            _oldHeight = height;
        }
    }

	void handleSync() {
		switch (stepSyncType) {
			case SyncFlush:
				glFlush();
				break;
			case SyncFinish:
				glFinish();
				break;
			default:
				break;
		}
	}

    void draw() override
    {
        {
            //Рендерим геометрию сцены в G-буфер
            auto query = gbufferTC.getQueryManager().beginQuery();
	        auto query2 = _countSamples ? gbufferSC.getQueryManager().beginQuery() : nullptr;
	        auto query3 = _countPrimitives ? gbufferPC.getQueryManager().beginQuery() : nullptr;
            drawToGBuffer(_gbufferFB, _renderToGBufferShader, _camera);
            if (query)
                query->endQuery();
	        if (query2)
		        query2->endQuery();
	        if (query3)
		        query3->endQuery();
        }

	    handleSync();

        //Рендерим геометрию сцены в теневую карту с позиции источника света
        {
            auto query = shadowTC.getQueryManager().beginQuery();
            drawToShadowMap(_shadowFB, _renderToShadowMapShader, _lightCamera);
            if (query)
                query->endQuery();
        }

	    handleSync();

        {
            //Выполняем отложенное освещение, заодно накладывает тени, а результат записываем в текстуру
            auto query = lightTC.getQueryManager().beginQuery();
            drawDeferred(_deferredFB, _renderDeferredShader, _camera, _lightCamera);
            if (query)
                query->endQuery();
        }

	    handleSync();

        {
            //Получаем текстуру с яркими областями
            if (_applyEffect || !_optimizeUnusedStages) {
                auto query = brightTC.getQueryManager().beginQuery();
                drawProcessTexture(_brightFB, _brightShader, _deferredTex);
                if (query)
                    query->endQuery();
            }
            else {
                brightTC.clearHistory();
            }
        }

	    handleSync();

        //Выполняем размытие текстуры с яркими областями
	    {
		    // horizontal blur
            if (_applyEffect || !_optimizeUnusedStages) {
                auto horizBlurTQ = horizBlurTC.getQueryManager().beginQuery();
                drawProcessTexture(_horizBlurFB, _optimizeBlur ? _horizBlurOptShader : _horizBlurShader, _brightTex);
                if (horizBlurTQ)
                    horizBlurTQ->endQuery();
            } else {
                horizBlurTC.clearHistory();
            }
	    }

	    handleSync();

	    {
		    // vertical blur
            if (_applyEffect || !_optimizeUnusedStages) {
                auto vertBlurTQ = vertBlurTC.getQueryManager().beginQuery();
                drawProcessTexture(_vertBlurFB, _optimizeBlur ? _vertBlurOptShader : _vertBlurShader, _horizBlurTex);
                if (vertBlurTQ)
                    vertBlurTQ->endQuery();
            }
            else {
                vertBlurTC.clearHistory();
            }
	    }

	    handleSync();

        // Final render with tonemapping.
        if (_applyEffect) {
            {
                auto query = tonemapTC.getQueryManager().beginQuery();
                drawToneMapping(_toneMappingFB, _toneMappingShader);
                if (query)
                    query->endQuery();
            }

	        handleSync();

            {
                auto query = finalRenderTC.getQueryManager().beginQuery();
                drawToScreen(_quadColorShader, _toneMappingTex);

                if (query)
                    query->endQuery();
            }
        } else {
            tonemapTC.clearHistory();

            auto query = finalRenderTC.getQueryManager().beginQuery();
            drawToScreen(_quadColorShader, _deferredTex);
            if (query)
                query->endQuery();
        }

        {
            //Отладочный рендер текстур
            auto query = debugTC.getQueryManager().beginQuery();
            drawDebug();

            if (query)
                query->endQuery();
        }
    }

    void drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0        
        glBindSampler(0, _repeatSampler);
        _brickTex->bind();
        shader->setIntUniform("diffuseTex", 0);

        drawScene(shader, camera);

        glUseProgram(0); //Отключаем шейдер

        fb->unbind(); //Отключаем фреймбуфер
    }

    void drawToShadowMap(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_FRONT);

        drawScene(shader, lightCamera);

        glDisable(GL_CULL_FACE);

        glUseProgram(0);

        fb->unbind();
    }

    void drawDeferred(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrixInverse", glm::inverse(camera.viewMatrix));
        shader->setMat4Uniform("projMatrixInverse", glm::inverse(camera.projMatrix));

        glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
        shader->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
        shader->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
        shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0        
        glBindSampler(0, _sampler);
        _normalsTex->bind();
        shader->setIntUniform("normalsTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1        
        glBindSampler(1, _sampler);
        _diffuseTex->bind();
        shader->setIntUniform("diffuseTex", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2        
        glBindSampler(2, _sampler);
        _depthTex->bind();
        shader->setIntUniform("depthTex", 2);

        glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3        
        glBindSampler(3, _depthSampler);
        _shadowTex->bind();
        shader->setIntUniform("shadowTex", 3);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawProcessTexture(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();

        shader->setFloatUniform("threshold", _brightnessThreshold);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        inputTexture->bind();
        shader->setIntUniform("tex", 0);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawToneMapping(const FramebufferPtr& fb, const ShaderProgramPtr& shader)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();

	    shader->setFloatUniform("bloomFactor", _bloomFactor);
        shader->setFloatUniform("exposure", _exposure);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _deferredTex->bind();
        shader->setIntUniform("tex", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler);
        _vertBlurTex->bind();
        shader->setIntUniform("bloomTex", 1);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        inputTexture->bind();
        shader->setIntUniform("tex", 0);

        glEnable(GL_FRAMEBUFFER_SRGB); //Включает гамма-коррекцию

        _quad->draw();

        glDisable(GL_FRAMEBUFFER_SRGB);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _cube->modelMatrix()))));

        _cube->draw();

        shader->setMat4Uniform("modelMatrix", _sphere->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _sphere->modelMatrix()))));

        _sphere->draw();

        shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ground->modelMatrix()))));

        _ground->draw();

        shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _bunny->modelMatrix()))));

        _bunny->draw();
    }

    void drawDebug()
    {
        int size = 500;

        if (_showGBufferDebug)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
            drawQuad(_quadDepthShader, _depthTex, 0, 0, size, size);
            drawQuad(_quadColorShader, _normalsTex, size, 0, size, size);
            drawQuad(_quadColorShader, _diffuseTex, size * 2, 0, size, size);
        }
        else if (_showShadowDebug)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
            drawQuad(_quadDepthShader, _shadowTex, 0, 0, size, size);
        }
        else if (_showDeferredDebug)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
            drawQuad(_quadColorShader, _deferredTex, 0, 0, size, size);
        }
        else if (_showBloomDebug)
        {
            glClear(GL_DEPTH_BUFFER_BIT);
            drawQuad(_quadColorShader, _brightTex, 0, 0, size, size);
            drawQuad(_quadColorShader, _horizBlurTex, size, 0, size, size);
            drawQuad(_quadColorShader, _vertBlurTex, size * 2, 0, size, size);
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawQuad(const ShaderProgramPtr& shader, const TexturePtr& texture, GLint x, GLint y, GLint width, GLint height)
    {
        glViewport(x, y, width, height);

        shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        texture->bind();
        shader->setIntUniform("tex", 0);

        _quad->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}