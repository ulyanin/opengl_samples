#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
	// Quad drawing.
	ShaderProgramPtr _shader;
	GLuint _quadVbo;
	GLuint _ubo;
	GLuint _vao;

	// Frustum drawing.
	ShaderProgramPtr _frustumShader;
	GLuint _frustumVbo;
	GLuint _frustumVao;

	// Frustum settings.
	struct {
		float near = 1;
		float far = 7;
		float side = 0.5f;
	} frustum;

	float invscale = 10;
	float postinvscale = 5;
	float colorScale = 1;

	void makeScene() override
	{
		Application::makeScene();

		// --- Для рисования квада.

		std::vector<float> points = {
				-1, -1,
				1, 1,
				1, -1,

				-1, -1,
				-1, 1,
				1, 1
		};

		glGenBuffers(1, &_quadVbo);
		glBindBuffer(GL_ARRAY_BUFFER, _quadVbo);
		glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(float), points.data(), GL_STATIC_DRAW);

		glGenVertexArrays(1, &_vao);
		glBindVertexArray(_vao);
		glBindBuffer(GL_ARRAY_BUFFER, _quadVbo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindVertexArray(0);

		_shader = std::make_shared<ShaderProgram>("shaders2/shaderQuad_matr.vert", "shaders2/colouredQuad.frag");

		// --- Для рисования пирамиды.

		glGenBuffers(1, &_frustumVbo);
		glBindBuffer(GL_ARRAY_BUFFER, _frustumVbo);
		glBufferData(GL_ARRAY_BUFFER, 2 * 4 * 2 * sizeof(float), nullptr, GL_STREAM_DRAW);

		glGenVertexArrays(1, &_frustumVao);
		glBindVertexArray(_frustumVao);
		glBindBuffer(GL_ARRAY_BUFFER, _frustumVbo);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindVertexArray(0);

		_frustumShader = std::make_shared<ShaderProgram>("shaders2/shaderQuad_matr.vert", "shaders2/simpleWhite.frag");

		updateFrustumBuffer(frustum.near, frustum.far, frustum.side);
	}

	void drawGUI() override {
		if (ImGui::CollapsingHeader("Frustum config")) {
			// name, val pointer, speed, min, max
			ImGui::DragFloat("Near plane", &frustum.near, 0.01f, 0.01f, frustum.far);
			ImGui::DragFloat("Far plane", &frustum.far, 0.1f, frustum.near + 0.01f, 1000);
			ImGui::DragFloat("Side", &frustum.side, 0.01f, 0.01f, 10.0f);
			ImGui::DragFloat("Inv scale", &invscale, 0.1f, 0.1f, 100.0f);
			ImGui::DragFloat("Post inv scale", &postinvscale, 0.1f, 0.1f, 100.0f);
			ImGui::DragFloat("Color scale", &colorScale, 0.05f, 0.1f, 20.0f);

			updateFrustumBuffer(frustum.near, frustum.far, frustum.side);
		}

		Application::drawGUI();
	}

	void update() override
	{
		Application::update();
	}

	glm::mat3 makeScaleMatrix(float scaleSide, float scaleDepth) {
		return glm::mat3(
			// Column 0
			scaleSide, 0, 0,
			// Column 1
			0, scaleDepth, 0,
			// Column 2
			0, 0, 1
		);
	}

	glm::mat3 makeOrthoMatrix(float near, float far, float side) {
		return glm::mat3(
			// Column 0
			2 / side, 0, 0,
			// Column 1
			0, -2/(far-near), 0,
			// Column 2
			0, -(far+near)/(far-near), 1
		);
	}

	glm::mat3 makeFrustumMatrix(float near, float far, float side) {
		// y x w
		return glm::mat3(
			// Column 0
			near / side, 0, 0,
			// Column 1
			0, -(far + near) / (far - near), -1,
			// Column 2
			0, -2 * far * near / (far - near), 0
		);
	}

	// Генерация контура пирамиды видимости в мировых координатах.
	void updateFrustumBuffer(float near, float far, float side) {
		float farside = far / near * side;

		std::vector<float> points = {
				near, side,
				far, farside,
				far, -farside,
				near, -side,

				-near, side,
				-far, farside,
				-far, -farside,
				-near, -side
		};

		glBindBuffer(GL_ARRAY_BUFFER, _frustumVbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * points.size(), points.data());
	}

	void drawQuad(glm::mat3 posmatrix, glm::mat3 colmatrix) {
		// Рисуем цветной квад.
		_shader->use();
		_shader->setMat3Uniform("posmatrix", posmatrix);
		_shader->setMat3Uniform("colmatrix", colmatrix);
		_shader->setFloatUniform("colorscale", colorScale);
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		_shader->setVec4Uniform("viewport", glm::vec4((float)viewport[0], (float)viewport[1], (float)viewport[2], (float)viewport[3]));
		glBindVertexArray(_vao);
		glDrawArrays(GL_TRIANGLES, 0, 2*3);
	}

	void drawFrustum(glm::mat3 matrix) {
		// Рисуем пирамиду видимости.
		_frustumShader->use();
		_frustumShader->setMat3Uniform("posmatrix", matrix);;
		_frustumShader->setMat3Uniform("colmatrix", glm::mat3(1));
		_frustumShader->setFloatUniform("colorscale", colorScale);
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		_frustumShader->setVec4Uniform("viewport", glm::vec4((float)viewport[0], (float)viewport[1], (float)viewport[2], (float)viewport[3]));
		glBindVertexArray(_frustumVao);
		glDrawArrays(GL_LINE_LOOP, 0, 2*2);
		glDrawArrays(GL_LINE_LOOP, 4, 2*2);
	}

	void draw() override
	{
		Application::draw();

		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glDisable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT);

		int border = 10;

		glm::mat3 frustumMatr = makeFrustumMatrix(frustum.near, frustum.far, frustum.side);

		// Матрицы для масштабирования мирового (левое) и пост-перспективного (усеченного) (правое) пространств.
		glm::mat3 scaleMatr = makeScaleMatrix(1/invscale, 1/invscale);
		glm::mat3 postScaleMatr = makeScaleMatrix(1/postinvscale, 1/postinvscale);

		// Первая половинка экрана.
		glViewport(0, 0, width / 2 - border, height);
		drawQuad(glm::mat3(1.0f), postScaleMatr * frustumMatr * glm::inverse(scaleMatr));
		drawFrustum(scaleMatr);

		// Вторая половинка экрана.
		glViewport(width / 2 + border, 0, width/2, height);
		drawQuad(glm::mat3(1), glm::mat3(1));
		drawFrustum(postScaleMatr * frustumMatr);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}